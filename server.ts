import {
	App
} from "https://deno.land/x/alosaur_lite/dist/mod.js"
import {homecontroller} from './home.controller.ts'


const app = new App({
  controllers: [homecontroller],
});

addEventListener("fetch", (event) => {
  event.respondWith(app.handleRequest(event.request));
});